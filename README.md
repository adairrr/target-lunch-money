# target-lunch-money

`target-lunch-money` is a Singer target for [Lunch Money](https://lunchmoney.app)
that can be used to update the balances of assets.

Built with [Singer SDK](https://gitlab.com/meltano/singer-sdk).

## Usage with Meltano

You can easily run `target-lunch-money` in a pipeline using [Meltano](https://www.meltano.com/).

### Installation

1. [Install Meltano](http://meltano.com/docs/getting-started.html#install-meltano):

    ```bash
    pip install meltano
    ```

3. [Create a project](http://meltano.com/docs/getting-started.html#create-your-meltano-project):

    ```bash
    meltano init target-lunch-money-playground
    cd target-lunch-money-playground
    ```

2. Add `target-lunch-money` to `meltano.yml` under `plugins`/`loaders` as a custom plugin:

    ```yaml
    plugins:
      loaders:
      - name: target-lunch-money
        namespace: target_lunch_money
        pip_url: git+https://gitlab.com/DouweM/target-lunch-money.git
        settings:
        - name: access_token
          kind: password
        - name: mapping
          kind: object
    ```

3. Install `target-lunch-money`:

    ```bash
    meltano install loader target-lunch-money
    ```

### Configuration

1. Acquire an access token via <https://my.lunchmoney.app/developers>.

2. Store the access token in `.env` using `meltano config`:

    ```bash
    meltano config target-lunch-money set access_token <ACCESS_TOKEN>
    ```

### Mapping

`target-lunch-money` expects an incoming `assets` stream with `name` and `balance` properties.
The `name` needs to be a substring of the corresponding asset's name in Lunch Money.
The `balance` is the asset's new `balance`.

If the incoming stream uses different naming, you can configure a mapping:

1. Add `mapping` to the `target-lunch-money` definition in `meltano.yml` under `config`:

    ```yaml
    plugins:
      loaders:
      - name: target-lunch-money
        # ...
        config:
          mapping:
            assets:
              in: assets # Incoming stream name for 'assets' is the same
              properties:
                name: name
                balance: price # Incoming property name for 'balance' is different
    ```

### Usage

#### Load only

1. Create an [`input.jsonl`](./input.jsonl) file with Singer tap output confirming the configured mapping.

2. Run `target-lunch-money` by itself using `meltano invoke`:

    ```bash
    cat input.jsonl | meltano invoke target-lunch-money
    ```

#### Extract & Load

1. Install and configure extractor [`tap-investing`](https://gitlab.com/DouweM/tap-investing#installation).

2. Run `target-lunch-money` in a pipeline with an extractor using `meltano elt`:

    ```bash
    meltano elt tap-investing target-lunch-money
    ```

## Stand-alone usage

### Installation

```bash
pip install git+https://gitlab.com/DouweM/target-lunch-money.git
```

### Configuration

1. Acquire an access token via <https://my.lunchmoney.app/developers>.

2. See the [Mapping](#mapping) section above for notes on mapping streams.

3. Create a [`config.json`](./config.jsonl) file with content along the following lines:

    ```json
    {
      "access_token": "<ACCESS_TOKEN>",
      "mapping": {
        "assets": {
          "in": "assets",
          "properties": {
            "name": "name",
            "balance": "price"
          }
        }
      }
    }
    ```

### Usage

1. Create an [`input.jsonl`](./input.jsonl) file with Singer tap output confirming the configured mapping.

2. Run `target-lunch-money` with the `config.json` file created above:

    ```bash
    cat input.jsonl | target-lunch-money --config config.json
    ```
