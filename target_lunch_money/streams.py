import singer

from .singer_sdk.stream import MappingStream
from .lunch_money import LunchMoney

logger = singer.get_logger()


class TargetLunchMoneyStream(MappingStream):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.lm = LunchMoney(self.target.config["access_token"])


class AssetsStream(TargetLunchMoneyStream):
    name = "assets"

    schema = {
        "name": {"type": "string"},
        "balance": {"type": "number"},
    }

    def persist_record(self, record):
        name = record["name"]
        balance = record["balance"]

        try:
            # TODO: Support other ways of matching than "substring"
            lm_asset = next(
                lm_asset
                for lm_asset in self.lm.list_assets()
                if name in lm_asset["name"]
            )
        except StopIteration:
            message = "Asset with name '{}' not found".format(name)
            if balance > 0:
                raise Exception(message)
            else:
                logger.info(message)
                return

        self.lm.update_asset_balance(lm_asset, balance)
        logger.info(
            "Updated '{}' balance from {} to {}".format(
                lm_asset["name"], lm_asset["balance"], balance
            )
        )
