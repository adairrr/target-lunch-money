import logging
import requests
import decimal
from typing import Dict, List


class LunchMoney:
    BASE_URL = "https://dev.lunchmoney.app/v1/"

    def __init__(self, token: str):
        self.token = token

    @property
    def _headers(self):
        return {"Authorization": f"Bearer {self.token}"}

    def _get(self, path: str) -> requests.Response:
        response = requests.get(
            f"{self.BASE_URL}{path}",
            headers=self._headers,
        )
        response.raise_for_status()
        return response

    def _put(self, path: str, payload: Dict):
        response = requests.put(
            f"{self.BASE_URL}{path}",
            headers=self._headers,
            json=payload,
        )
        response.raise_for_status()
        return response

    def list_assets(self) -> List[Dict]:
        return self._get("assets").json()["assets"]

    def update_asset_balance(self, asset: Dict, new_balance: decimal.Decimal):
        id = asset["id"]
        balance = decimal.Decimal(asset["balance"])

        if new_balance != balance:
            self._put(f"assets/{id}", {"balance": str(new_balance)})
