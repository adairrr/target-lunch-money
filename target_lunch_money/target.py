import io

import click
import json
import sys

from .singer_sdk.target import MappingTarget
from .streams import AssetsStream

STREAM_TYPES = [
    AssetsStream,
]


class TargetLunchMoney(MappingTarget):
    def streams(self):
        return [stream_class(target=self) for stream_class in STREAM_TYPES]


cli = TargetLunchMoney.cli()
